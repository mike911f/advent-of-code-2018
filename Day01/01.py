####################################################################
####                    ADVENT OF CODE 2018                     ####
####                        Michael Jurek                       ####
####                            Day 1/1                         ####
####################################################################

def count(file):
    pom = 0
    for line in file:
        if not line == '\n':
            pom += int(line)

    return pom 

def main():
    try:
        file = open("input1.txt","r")
        print(count(file))
    finally:
        file.close()

if __name__ == '__main__':
    main()