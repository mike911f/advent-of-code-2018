####################################################################
####                    ADVENT OF CODE 2018                     ####
####                        Michael Jurek                       ####
####                            Day 1/2                         ####
####################################################################

def findDuplicates(lst, reg, file): 
    for value in lst:
        if value in reg:
            file.close()
            print(value)
            return value
        else:
            reg[value] = 1

    counter(lst[-1], reg, file, lst=[])


def counter(pom, register, file=None, lst=None):   
    file = open("input1.txt","r")
    for line in file:
        if line != '\n':
            pom += int(line)
            lst.append(pom)
    return findDuplicates(lst, register, file)

def main():
    reg = {}
    pom = 0
    lst = []
    lst.append(pom)
    counter(pom, reg, lst=lst)

if __name__ == '__main__':
    main()