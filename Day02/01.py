####################################################################
####                    ADVENT OF CODE 2018                     ####
####                        Michael Jurek                       ####
####                            Day 2/1                         ####
####################################################################

from collections import Counter

def count(content):
    pom1 = 0
    pom2 = 0
    for item in content:
        pokus = Counter(item).most_common(2)
        if pokus[0][1] == 3 and  pokus[1][1] == 2:
            pom1 += 1
            pom2 += 1
        elif pokus[0][1] == 3 and pokus[1][1] != 2:
            pom1 += 1
        elif pokus[0][1] == 2:
            pom2 += 1

    return pom1 * pom2

def main():
    try:
        file = open("input.txt","r")
        lines = file.readlines()

        content = [x.strip() for x in lines]
        result = count(content)
        print(result)

    finally:
        file.close()

if __name__ == '__main__':
    main()