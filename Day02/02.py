####################################################################
####                    ADVENT OF CODE 2018                     ####
####                        Michael Jurek                       ####
####                            Day 2/2                         ####
####################################################################

def count(content):
    reg = []
    var = 0
    for item in content:
        var = len(item)
        content.remove(item)
        common_char = ''
        pom =[]

        for i in content:
            for s in range(len(i)):
                if item[s] == i[s]:
                    common_char += i[s]
            pom.append(common_char)
            common_char = ''
        reg.append(max(pom, key=len)) 

    for item in reg:
        if (var-1) == len(item):
            return item

def main():
    try:
        file = open("input.txt","r")
        lines = file.readlines()

        content = [x.strip() for x in lines]
        result = count(content)
        print(result)

    finally:
        file.close()

if __name__ == '__main__':
    main()